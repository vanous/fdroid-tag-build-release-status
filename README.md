A very crude proof of concept, to see status of the "tag → wait → release".

## Not real data, demo only:

Gadgetbridge:

<img src=demo/tagged.png> <img src=demo/waiting.png><br/>
<img src=demo/buildbot_version.png> <img src=demo/buildbot_version_code.png> <img src=demo/last.png><br/>
<img src=demo/fdroid.png><br/>

### Note: Gittea cannot show SVGs correctly yet(*), so examples are svgs converted to converted to png. 

* https://github.com/go-gitea/gitea/pull/14101
