#!/bin/python

import requests
import json
import os
import sys
import datetime
import yaml
import bs4
import anybadge

server = "https://codeberg.org/api/v1"
owner = "Freeyourgadget"
repo = "Gadgetbridge"
repos = "repos"
endpoint = "tags"
package_name = "nodomain.freeyourgadget.gadgetbridge"
yamlurl = f"https://gitlab.com/fdroid/fdroiddata/-/raw/master/metadata/{package_name}.yml?inline=true"
lastbuildlogurl = f"https://f-droid.org/wiki/page/{package_name}/lastbuild"
fdroid=f"https://f-droid.org/packages/{package_name}/"
fdroid=f"https://f-droid.org/api/v1/packages/{package_name}"

build_number = None
build_status = None
tagged_version = None
tagged_date = None
metadata_version = None
metadata_version_code = None
fdroid_version=None

def get_tags_data():
    url = f"{server}/{repos}/{owner}/{repo}/{endpoint}"
    r = requests.get(url)
    if r.status_code == requests.codes.ok:
        data = json.loads(r.content)
        return data


def get_yaml_data():
    url = yamlurl
    r = requests.get(url)
    if r.status_code == requests.codes.ok:
        data = r.content
        return data

def get_build_log_data():
    url = lastbuildlogurl
    r = requests.get(url)
    if r.status_code == requests.codes.ok:
        data = r.text
        return data

def get_fdroid_data():
    url = fdroid
    r = requests.get(url)
    if r.status_code == requests.codes.ok:
        data = json.loads(r.content)
        return data


def get_tag_details(data):
    tag = data[0]
    commit = tag["commit"]
    version = tag["name"]
    created = commit["created"]
    date = datetime.datetime.fromisoformat(created)  # 2020-12-21T17:17:30+01:00
    return (version, date)


def get_yaml_details(data):
    yamldata = yaml.load(data, Loader=yaml.Loader)
    return (yamldata["CurrentVersion"], yamldata["CurrentVersionCode"])

def get_build_details(data):
    soup = bs4.BeautifulSoup(data, features="lxml")
    status = None
    number = None
    span = soup.find(id="Build_Log")
    status = span.parent.find_next_siblings("p")[0].text
    number = int(soup.find("h1").text.split()[1])
    return (status, number)

def get_fdroid_details(data):
    soup = bs4.BeautifulSoup(data, features="lxml")
    version = None #e.g 0.52.0
    latest=soup.find("li", {"id":"latest"})
    if latest is None:
        print("Error in f-droid parsing")
    header = latest.find('div', {'class':'package-version-header'}) #find_next_siblings()[0] #not needed now
    version = header.find('a').attrs["name"] #because version is first
    return (version)

def get_fdroid_details_from_json(data):
    version = data['packages'][0]['versionName'] #e.g. 0.52.0
    return (version)


if __name__ == "__main__":
    tagged_version, tagged_date = get_tag_details(get_tags_data())
    metadata_version, metadata_version_code = get_yaml_details(get_yaml_data())
    build_status, build_number = get_build_details(get_build_log_data())
    fdroid_version=get_fdroid_details_from_json(get_fdroid_data())

    if build_number is not None:
        color = "red"
        if "Build succeeded" in build_status:
            color = "orange"
            if metadata_version_code==build_number:
                color = "green"

        badge = anybadge.Badge(
            label=f"{build_status}", value=build_number, default_color=color
        )
        badge.write_badge("last.svg", overwrite=True)

        badge = anybadge.Badge(label="Buildbot version", value=metadata_version)
        badge.write_badge("buildbot_version.svg", overwrite=True)

        badge = anybadge.Badge(
            label="Buildbot version code", value=metadata_version_code
        )
        badge.write_badge("buildbot_version_code.svg", overwrite=True)

    if tagged_date is not None:
        now = datetime.datetime.now(datetime.timezone.utc)
        diff = (now - tagged_date).days
        badge = anybadge.Badge(label="Tagged days ago:", value=diff)
        badge.write_badge("waiting.svg", overwrite=True)

    if tagged_version is not None:
        badge = anybadge.Badge(label="Tagged version", value=tagged_version)
        badge.write_badge("tagged.svg", overwrite=True)

    if fdroid_version is not None:
        color = "green"
        if fdroid_version != tagged_version:
            color = "red"

        badge = anybadge.Badge(
            label="Fdroid version", value=fdroid_version, default_color=color
        )
        badge.write_badge("fdroid.svg", overwrite=True)
