#!/bin/python

import json
import os
import sys
import datetime
import yaml
import bs4
import anybadge
import asyncio
import httpx

server = "https://codeberg.org/api/v1"
owner = "Freeyourgadget"
repo = "Gadgetbridge"
repos = "repos"
endpoint = "tags"
package_name = "nodomain.freeyourgadget.gadgetbridge"
yamlurl = f"https://gitlab.com/fdroid/fdroiddata/-/raw/master/metadata/{package_name}.yml?inline=true"
lastbuildlogurl = f"https://f-droid.org/wiki/page/{package_name}/lastbuild"
fdroid=f"https://f-droid.org/packages/{package_name}/"

build_number = None
build_status = None
tagged_version = None
tagged_date = None
metadata_version = None
metadata_version_code = None
fdroid_version=None
timeout = httpx.Timeout(60.0, connect=300.0)


async def make_request(url):
    r=None
    tries=5
    async with httpx.AsyncClient() as client:
        r = await client.get(url, timeout=timeout)
    return r


async def get_tags_data():
    url = f"{server}/{repos}/{owner}/{repo}/{endpoint}"
    r = await make_request(url)
    data = json.loads(r.content)
    return data


async def get_yaml_data():
    url = yamlurl
    r = await make_request(url)

    data = r.content
    return data

async def get_build_log_data():
    url = lastbuildlogurl
    r = await make_request(url)

    data = r.text
    return data

async def get_fdroid_data():
    url = fdroid
    r = await make_request(url)

    data = r.text
    return data


def get_tag_details(data):
    tag = data[0]
    commit = tag["commit"]
    version = tag["name"]
    created = commit["created"]
    date = datetime.datetime.fromisoformat(created)  # 2020-12-21T17:17:30+01:00
    tagged_date=date
    tagged_version=version


def get_yaml_details(data):
    yamldata = yaml.load(data, Loader=yaml.Loader)
    metadata_version = yamldata["CurrentVersion"]
    metadata_version_code=yamldata["CurrentVersionCode"]
    

def get_build_details(data):
    soup = bs4.BeautifulSoup(data, features="lxml")
    status = None
    number = None
    span = soup.find(id="Build_Log")
    status = span.parent.find_next_siblings("p")[0].text
    number = soup.find("h1").text.split()[1]
    build_status=status
    build_number=number

def get_fdroid_details(data):
    soup = bs4.BeautifulSoup(data, features="lxml")
    version = None
    suggested=soup.find("a", {"name":"suggested"}).find_next_siblings()[0]
    version = suggested.find('a').attrs["name"]
    fdroid_version=version

async def do_all():
    task_list = []
    task_list.append(get_tag_details(await get_tags_data()))
    task_list.append(get_yaml_details(await get_yaml_data()))
    task_list.append(get_build_details(await get_build_log_data()))
    task_list.append(get_fdroid_details(await get_fdroid_data()))
    await asyncio.gather(*task_list)
    if build_number is not None:
        color = "green"
        if build_status == "Build suceeded":
            color = "red"

        badge = anybadge.Badge(
            label=f"{build_status}", value=build_number, default_color=color
        )
        badge.write_badge("last.svg", overwrite=True)

        badge = anybadge.Badge(label="Buildbot version", value=metadata_version)
        badge.write_badge("buildbot_version.svg", overwrite=True)

        badge = anybadge.Badge(
            label="Buildbot version code", value=metadata_version_code
        )
        badge.write_badge("buildbot_version_code.svg", overwrite=True)

    if tagged_date is not None:
        now = datetime.datetime.now(datetime.timezone.utc)
        diff = (now - tagged_date).days
        badge = anybadge.Badge(label="Tagged days ago:", value=diff)
        badge.write_badge("waiting.svg", overwrite=True)

    if tagged_version is not None:
        badge = anybadge.Badge(label="Tagged version", value=tagged_version)
        badge.write_badge("tagged.svg", overwrite=True)

    if fdroid_version is not None:
        color = "green"
        if fdroid_version != tagged_version:
            color = "red"

        badge = anybadge.Badge(
            label="Fdroid version", value=fdroid_version, default_color=color
        )
        badge.write_badge("fdroid.svg", overwrite=True)

        badge = anybadge.Badge(
                label="Generated", value=datetime.datetime.now().strftime("%d/%m/%Y %H:%M") 
            , default_color="green"
        )
        badge.write_badge("generated.svg", overwrite=True)

if __name__ == "__main__":
    asyncio.run(do_all())
